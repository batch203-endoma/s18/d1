function printInfo() {
  let nickName = prompt("Enter your nickname");
  console.log("Hi, " + nickName);
}

// printInfo()

function printName(name) {
  console.log("My name is " + name);

}

printName("Juana");
printName("John");

// [SECTION] Parameters and arguments

// Parameter
  //"name" is called a parameter
  // A "parameter acts a named variable/container that exists only inside the function"
  // It is used to store information that is provided to function when it is called/invoke.

  // arguments
    // "Juana" and "John", the information/data provided directly into the function is called "argument".
    // Values passed when invoking a function are called arguments.
    // These arguments are then stored as the parameter within the function.

  // Vriables can also be passed as an argument.

  let sampleVariable ="Yui";


  printName(sampleVariable);
  printName(); //Funciton calling without arguments will result to undefined parameters.


// Check if a specific number is divisible by 8

function checkDivisibilityBy8(num){
  let remainder = num %  8;
  console.log("The remainder of " + num + " divided by 8 is " + remainder);
  let isDivisibleBy8 = remainder === 0;
  console.log("Is " + num + " divisible by 8?");
  console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);

// [Section] function as argument
  // function parameters can also accept other functions as arguments.
  // Some complex functions uses other functions as arguments to perfor more complicated results.

  function argumentFunction(){
    console.log("This function was passed as an argument before the message was printed.");
    }

    function invokeFunction(argumentFunction){
      argumentFunction();
    }

    // A Function used without a parenthesis is normally associated with using the function as  an argument to another function.
    invokeFunction(argumentFunction);

    console.log(argumentFunction);

    // [SECTION] using multiple Parameters
    // Multiple "arguments" will corresponds to the number of "parameters" declared in a function in succeeding order.

    function createFullName(firstName, middleName, lastName){
      console.log("My full name is " + firstName + " " + middleName+ " " + lastName);
    }

    createFullName("Angelito", "Quiambao"); //undefined parameters will be left undefined. no error.

    let firstName = "John";
    let middleName = "Doe";
    let lastName = "Smith";

    createFullName (firstName, middleName, lastName);

    // [SECTION] The return statement
      // The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called.

      function returnFullName(firstName, middleName, lastName) {
        return firstName + " " + middleName + " " + lastName;
        //This line of code will not be printed.
        console.log("This is printed inside the function");

      }

    // Value returned from the function is stored in our Variable.
    let completeName = returnFullName ("Jeffrey" , "Smith", "Bezos");

    console.log(completeName);

    // Using return statement value, we can further use/manipulate in our program instead of only printing/displaying in our console log.
    console.log("My name is "+completeName);

    console.log(returnFullName ("Jeffrey" , "Smith", "Bezos"));

    // When a function only have console.log() it result  will return undefined.
    function printPlayerInfo(userName, level, job) {
      console.log("username : " +userName);
      console.log("Level : " + level);
      console.log("Job: " + job);

    }

    let user1 = printPlayerInfo ("knight_white", 95, "Paladin");
    console.log(user1);

    // Return is for storing information.























  //
